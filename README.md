# DTI metrics

This is a simple tool for opening MRI DTI sequences, delineating ROIs and writing FA, Trace, AD, RD and MD in tables.