function ROIs = DTIroimanager(IMAGE,ROIs)
%% DEBUG
%ROIs = ALLROIS{imgidx}(slice+1);



% Initialize
proceed = true;
ROIs=ROIs{1};

while proceed
    % Draw image and ROIs (if any)
    imagesc(IMAGE);
    colormap(pink)
    NROIs=length(ROIs);
    if NROIs==0 % No ROIs
    else % Display ROIs
        hold on
        for roi=1:NROIs % Draw ROIS
            x = ROIs{roi}.x;
            y = ROIs{roi}.y;
            plot(x,y,'LineWidth',5)
        end
        for roi=1:NROIs % Add text
            name = cell2mat(ROIs{roi}.name);
            x = ROIs{roi}.x;
            y = ROIs{roi}.y;
            text(median(x),median(y),name,'Color','white','FontSize',12)
        end
        hold off
    end
    answer = questdlg('What do you want to do?','ROI manager','Add ROI','Delete ROI','Return to slice selection','Add ROI'); % Prompt for question
    switch answer
        case 'Add ROI'
            NROIs=length(ROIs);
            ROI = [];
            [~,ROI.x,ROI.y] = roipoly;
            ROI.name = inputdlg('Please specify ROI name:');
            ROIs{NROIs+1}=ROI;
        case 'Delete ROI'
            NROIs=length(ROIs);
            ROInames=[];
            for roi=1:NROIs
                ROInames{roi} = cell2mat(ROIs{roi}.name);
            end
            idx=listdlg('PromptString','Select ROI(s) to delete','ListString',ROInames);
            ROIs(idx)=[];
        case 'Return to slice selection' % End here
            proceed = false;
    end
end
close;

end