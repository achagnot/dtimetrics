function DTImetrics()
% HOW TO USE
% Just type DTImetrics in the console


% Step 1: load file names and paths
[files,path] = uigetfile('.nii','Multiselect','on');
creatingROIs = true;
% Preallocating ALLROIS
ALLROIS=cell(0);
for nsub=1:length(files)
    ALLROIS{nsub}=cell(23,1);
end
while creatingROIs
    action = questdlg('What do you want to do with these files?','DTI processing menu','Create/Edit ROI set','Analyze created ROIs','Create/Edit ROI set');
    switch action
        case 'Create/Edit ROI set'
            drawingROIs=true;
            % select image to open
            sub = listdlg('PromptString',{'Create ROIs for which image?','Names starting with * already have ROIs'},'ListString',files,'SelectionMode','single');
            % import and reshape properly
            IMAGE = niftiread(fullfile(path,erase(files{sub},'*')));
            IMAGE = squeeze(IMAGE);
            IMAGE = rot90(IMAGE);
            IMAGE = reshape(IMAGE,size(IMAGE,1),size(IMAGE,2),23,23); % X Y Z C
            while drawingROIs
                action = questdlg('ROI drawing','Slice selection','Slice selection','Exit','Slice selection');
                switch action
                    case 'Slice selection' % Select slice
                       slice = listdlg('PromptString','Select slice','ListString',{'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23'},'SelectionMode','single');
                       SLICE=IMAGE(:,:,slice,1);
                       ALLROIS{sub}(slice) = {DTIroimanager(SLICE,ALLROIS{sub}(slice))};
                    case 'Exit' % End ROI drawing
                        drawingROIs=false;
                        files{sub} = strcat('*',erase(files{sub},'*'));
                end
            end
        case 'Analyze created ROIs'
            creatingROIs=false;
            ALLDATA=[];
            ALLIDs = cell(0);
            roicount=0;
            for sub=1:length(files)
                IMAGE = niftiread(fullfile(path,erase(files{sub},'*')));
                IMAGE = squeeze(IMAGE);
                IMAGE = rot90(IMAGE);
                IMAGE = reshape(IMAGE,size(IMAGE,1),size(IMAGE,2),23,23); % X Y Z C
                IMAGE = double(IMAGE);
                for slice=1:23
                    ROIs = ALLROIS{sub}(slice);
                    ROIs = ROIs{1};
                    if isempty(ROIs)
                    else
                        NROIs=length(ROIs);
                        SLICE = IMAGE(:,:,slice,1);
                        imshow(SLICE)
                        [~,subname,~]=fileparts(fullfile(path,erase(files{sub},'*')));
                        for roi=1:NROIs
                            roicount=roicount+1;
                            ROI = ROIs{roi};
                            coords=cat(2,ROI.x,ROI.y);
                            h=impoly(gca,coords);
                            MASK=h.createMask;
                            MASK=double(MASK);
                            MASK(MASK==0)=NaN;
                            % Fill in metrics
                            ALLDATA(roicount,1)= median(IMAGE(:,:,slice,1).*MASK,'all','omitnan');% FA (1)
                            ALLDATA(roicount,2)= median(IMAGE(:,:,slice,2).*MASK,'all','omitnan');% Trace (2)
                            ALLDATA(roicount,3)= median(IMAGE(:,:,slice,11).*MASK,'all','omitnan');% AD (11)
                            ALLDATA(roicount,4)= median(IMAGE(:,:,slice,12).*MASK,'all','omitnan') + median(IMAGE(:,:,slice,13).*MASK,'all','omitnan');% RD (12+13)
                            ALLDATA(roicount,5)= ALLDATA(roicount,3) + ALLDATA(roicount,4);% MD (11+12+13)
                            ALLIDs{roicount}=char(strcat(subname,'_','slice#',num2str(slice),'_',ROI.name));
                        end
                        close()
                    end
                end
            end % end of ROI analysis
            % Save data
            TABLE = table(ALLDATA');
            TABLE=splitvars(TABLE);
            TABLE.Properties.VariableNames = ALLIDs; % TROUBLE HERE
            Vars = {'FA';'Trace';'Axial Diffusivity';'Radial Diffusivity';'Mean Diffusivity'};
            TABLE = addvars(TABLE,Vars,'Before',1);
            writetable(TABLE,'DTImetrics.csv','Delimiter',',');
            save('ROIs.mat','ALLROIS');
    end
end
end